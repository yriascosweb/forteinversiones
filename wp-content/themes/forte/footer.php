<?php
/**
 * @package WordPress
 * @subpackage Imperio
 */
?>
	
	<div id="big_footer" <?php if (get_option("imperio_footer_full_width") == "on") echo " class='footer-full-width'"; ?>>

		<?php
		if (get_option("imperio_show_primary_footer") == "on"){
			?>
			<div id="primary_footer">
			    	<div class="<?php if (get_option("imperio_footer_full_width") == "off") echo "container"; ?> no-fcontainer">
			    	<?php
			    	/* Show Newsletter in Footer */
					if (get_option("imperio_newsletter_enabled") == "on"){
						$code = get_option("imperio_mailchimp_code");
						if (!empty($code)){
						    $output = '<div class="newsletter_shortcode footer_newsletter"><div class="mail-box"><div class="mail-news"><div class="news-l"><div class="banner"><h3>'.sprintf(esc_html("%s", "imperio"), get_option("imperio_newsletter_text")).'</h3><p>'.sprintf(esc_html("%s", "imperio"), get_option("imperio_newsletter_stext")).'</p></div><div class="form">';
						    if (function_exists('icl_t')){
							    $output = '<div class="newsletter_shortcode"><div class="mail-box"><div class="mail-news"><div class="news-l"><div class="banner"><h3>'.wp_kses_post(icl_t( 'imperio', 'Subscribe our <span class=text_color>Newsletter</span>', get_option('imperio_newsletter_text'))).'</h3><p>'.wp_kses_post(icl_t( 'imperio', 'Subscribe to our newsletter to receive news, cool free stuff updates and new released products (no spam!)', get_option('imperio_newsletter_stext'))).'</p></div><div class="form">';
						    }
							$output .= stripslashes($code);
							$output .= '</div></div></div></div></div>';			
						} else {
							$output = '<div class="newsletter_shortcode">'.esc_html__('You need to fill the inputs on the Appearance > Imperio Options > Newsletter panel in order to work.','imperio').'</div>';
						}			
						$output = wp_kses_no_null( $output, array( 'slash_zero' => 'keep' ) );
						$output = wp_kses_normalize_entities($output);
						echo wp_kses_hook($output, 'post', array()); // WP changed the order of these funcs and added args to wp_kses_hook		
					
					}?>	
	    		<?php
	    		
					if(get_option("imperio_footer_number_cols") == "one"){ ?>
						<div class="footer_sidebar col-xs-12 col-md-12"><?php imperio_print_sidebar('footer-one-column'); ?></div>
					<?php }
					if(get_option("imperio_footer_number_cols") == "two"){ ?>
						<div class="footer_sidebar col-xs-12 col-md-6"><?php imperio_print_sidebar('footer-two-column-left'); ?></div>
						<div class="footer_sidebar col-xs-12 col-md-6"><?php imperio_print_sidebar('footer-two-column-right'); ?></div>
					<?php }
					if(get_option("imperio_footer_number_cols") == "three"){
						if(get_option("imperio_footer_columns_order") == "one_three"){ ?>
							<div class="footer_sidebar col-xs-12 col-md-4"><?php imperio_print_sidebar('footer-three-column-left'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-4"><?php imperio_print_sidebar('footer-three-column-center'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-4"><?php imperio_print_sidebar('footer-three-column-right'); ?></div>
						<?php }
						if(get_option("imperio_footer_columns_order") == "one_two_three"){ ?>
							<div class="footer_sidebar col-xs-12 col-md-4"><?php imperio_print_sidebar('footer-three-column-left-1_3'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-8"><?php imperio_print_sidebar('footer-three-column-right-2_3'); ?></div>
						<?php }
						if(get_option("imperio_footer_columns_order") == "two_one_three"){ ?>
							<div class="footer_sidebar col-xs-12 col-md-8"><?php imperio_print_sidebar('footer-three-column-left-2_3'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-4"><?php imperio_print_sidebar('footer-three-column-right-1_3'); ?></div>
						<?php }
					}
					if(get_option("imperio_footer_number_cols") == "four"){
						if(get_option("imperio_footer_columns_order_four") == "one_four"){ ?>
							<div class="footer_sidebar col-xs-12 col-md-3"><?php imperio_print_sidebar('footer-four-column-left'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-3"><?php imperio_print_sidebar('footer-four-column-center-left'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-3"><?php imperio_print_sidebar('footer-four-column-center-right'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-3"><?php imperio_print_sidebar('footer-four-column-right'); ?></div>
						<?php }
						if(get_option("imperio_footer_columns_order_four") == "two_one_two_four"){ ?>
							<div class="footer_sidebar col-xs-12 col-md-3"><?php imperio_print_sidebar('footer-four-column-left-1_2_4'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-6"><?php imperio_print_sidebar('footer-four-column-center-2_2_4'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-3"><?php imperio_print_sidebar('footer-four-column-right-1_2_4'); ?></div>
						<?php }
						if(get_option("imperio_footer_columns_order_four") == "three_one_four"){ ?>
							<div class="footer_sidebar col-xs-12 col-md-8"><?php imperio_print_sidebar('footer-four-column-left-3_4'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-4"><?php imperio_print_sidebar('footer-four-column-right-1_4'); ?></div>
						<?php }
						if(get_option("imperio_footer_columns_order_four") == "one_three_four"){ ?>
							<div class="footer_sidebar col-xs-12 col-md-4"><?php imperio_print_sidebar('footer-four-column-left-1_4'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-8"><?php imperio_print_sidebar('footer-four-column-right-3_4'); ?></div>
						<?php }
					}
				?>
				</div>
		    </div>
			<?php
		}
	?>
    
    <?php
	    if (get_option("imperio_show_sec_footer") == "on"){
		    $imperio_is_only_custom_text = (get_option("imperio_footer_display_logo") != 'on' && get_option("imperio_footer_display_social_icons") != "on") ? true : false;
		    ?>
		    <div id="secondary_footer">
				<div class="container <?php echo $imperio_is_only_custom_text ? "only_custom_text" : ""; ?>">
					
					<?php
						/* FOOTER LOGOTYPE */
						if (get_option("imperio_footer_display_logo") == 'on'){
						?>
						<a class="footer_logo align-<?php echo esc_attr(get_option("imperio_footer_logo_alignment")); ?>" href="<?php echo esc_url(home_url("/")); ?>" tabindex="-1">
				        	<?php
				    			$alone = true;
			    				if (get_option("imperio_footer_logo_retina_image_url") != ""){
				    				$alone = false;
			    				}
		    					?>
		    					<img class="footer_logo_normal <?php if (!$alone) echo "notalone"; ?>" style="position: relative;" src="<?php echo esc_url(get_option("imperio_footer_logo_image_url")); ?>" alt="<?php esc_attr_e("", "imperio"); ?>" title="<?php esc_attr_e("", "imperio"); ?>">
			    					
			    				<?php 
			    				if (get_option("imperio_footer_logo_retina_image_url") != ""){
			    				?>
				    				<img class="footer_logo_retina" style="display:none; position: relative;" src="<?php echo esc_url(get_option("imperio_footer_logo_retina_image_url")); ?>" alt="<?php esc_attr_e("", "imperio"); ?>" title="<?php esc_attr_e("", "imperio"); ?>">
			    				<?php
		    					}
			    			?>
				        </a>
						<?php
						}
						
						/* FOOTER CUSTOM TEXT */
						if (get_option("imperio_footer_display_custom_text") == "on"){
						?>
						<div class="footer_custom_text <?php echo $imperio_is_only_custom_text ? "wide" : esc_attr(get_option("imperio_footer_custom_text_alignment")); ?>"><?php echo do_shortcode(stripslashes(get_option("imperio_footer_custom_text"))); ?></div>
						<?php
						}
						
						/* FOOTER SOCIAL ICONS */
						if (get_option("imperio_footer_display_social_icons") == "on"){
						?>
						<div class="social-icons-fa align-<?php echo esc_attr(get_option("imperio_footer_social_icons_alignment")); ?>">
					        <ul>
							<?php
								$icons = array(array("facebook","Facebook"),array("twitter","Twitter"),array("tumblr","Tumblr"),array("stumbleupon","Stumble Upon"),array("flickr","Flickr"),array("linkedin","LinkedIn"),array("delicious","Delicious"),array("skype","Skype"),array("digg","Digg"),array("google-plus","Google+"),array("vimeo-square","Vimeo"),array("deviantart","DeviantArt"),array("behance","Behance"),array("instagram","Instagram"),array("wordpress","WordPress"),array("youtube","Youtube"),array("reddit","Reddit"),array("rss","RSS"),array("soundcloud","SoundCloud"),array("pinterest","Pinterest"),array("dribbble","Dribbble"),array("vk","Vk"),array("yelp","Yelp"),array("twitch","Twitch"),array("houzz","Houzz"),array("foursquare","Foursquare"),array("slack","Slack"),array("whatsapp","Whatsapp"));
								foreach ($icons as $i){
									if (is_string(get_option("imperio_icon-".$i[0])) && get_option("imperio_icon-".$i[0]) != ""){
									?>
									<li>
										<a href="<?php echo esc_url(get_option("imperio_icon-".$i[0])); ?>" target="_blank" class="<?php echo esc_attr(strtolower($i[0])); ?>" title="<?php echo esc_attr($i[1]); ?>"><i class="fa fa-<?php echo esc_attr(strtolower($i[0])); ?>"></i></a>
									</li>
									<?php
									}
								}
							?>
						    </ul>
						</div>
						<?php
						}
						
						
					?>
				</div>
			</div>
		    <?php
	    }
    ?>
	</div>

<?php

/* sets the type of pagination [scroll, autoscroll, paged, default] */
wp_reset_query();
$imperio_reading_option = get_option('imperio_blog_reading_type');
if (is_archive() || is_single() || is_search() || is_page_template('blog-template.php') || is_page_template('blog-masonry-template.php')) {

	$nposts = get_option('posts_per_page');

	$imperio_more = 0;
	$imperio_pag = 0;

	$orderby="";
	$category="";
	$nposts = "";
	$order = "";

	$imperio_pag = $wp_query->query_vars['paged'];
	if (!is_numeric($imperio_pag)) $imperio_pag = 1;
	$max = 0;

	switch ($imperio_reading_option){
		case "scrollauto": 

				// Add code to index pages.
				if( !is_singular() ) {	

					if (is_search()){

						$imperio_reading_option = get_option('imperio_blog_reading_type');
						$se = get_option("imperio_enable_search_everything");

						$nposts = get_option('posts_per_page');

						$imperio_pag = $wp_query->query_vars['paged'];
						if (!is_numeric($imperio_pag)) $imperio_pag = 1;

						if ($se == "on"){
							$args = array(
								'showposts' => get_option('posts_per_page'),
								'post_status' => 'publish',
								'paged' => $imperio_pag,
								's' => esc_html($_GET['s'])
							);

						    $imperio_the_query = new WP_Query( $args );

						    $args2 = array(
								'showposts' => -1,
								'post_status' => 'publish',
								'paged' => $imperio_pag,
								's' => esc_html($_GET['s'])
							);

							$counter = new WP_Query($args2);

						} else {
							$args = array(
								'showposts' => get_option('posts_per_page'),
								'post_status' => 'publish',
								'paged' => $imperio_pag,
								'post_type' => 'post',
								's' => esc_html($_GET['s'])
							);

						    $imperio_the_query = new WP_Query( $args );

						    $args2 = array(
								'showposts' => -1,
								'post_status' => 'publish',
								'paged' => $imperio_pag,
								'post_type' => 'post',
								's' => esc_html($_GET['s'])
							);

							$counter = new WP_Query($args2);
						}

						$max = ceil($counter->post_count / $nposts);
						$imperio_paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;

					} else {

						$max = $wp_query->max_num_pages;
						$imperio_paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;

					}
					
					$imperio_inline_script = '
						jQuery(document).ready(function($){
							"use strict";
							if (jQuery("#reading_option").html() === "scrollauto" && !jQuery("body").hasClass("single")){ 
								window.imperio_loadingPoint = 0;
								//monitor page scroll to fire up more posts loader
								window.clearInterval(window.imperio_interval);
								window.imperio_interval = setInterval("imperio_monitorScrollTop()", 1000 );
							}
						});
					';
					wp_add_inline_script('imperio-global', $imperio_inline_script, 'after');

				} else {

				    $args = array(
	    				'showposts' => $nposts,
	    				'orderby' => $orderby,
	    				'order' => $order,
	    				'cat' => $category,
	    				'paged' => $imperio_pag,
	    				'post_status' => 'publish'
	    			);

	    		    $imperio_the_query = new WP_Query( $args );

		    		$max = $imperio_the_query->max_num_pages;
		    		$imperio_paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;

		    		$imperio_inline_script = '
						jQuery(document).ready(function($){
							"use strict";
							if (jQuery("#reading_option").html() === "scrollauto" && !jQuery("body").hasClass("single")){ 
								window.imperio_loadingPoint = 0;
								//monitor page scroll to fire up more posts loader
								window.clearInterval(window.imperio_interval);
								window.imperio_interval = setInterval("imperio_monitorScrollTop()", 1000 );
							}
						});
					';
					wp_add_inline_script('imperio-global', $imperio_inline_script, 'after');

	    		}
			break;
		case "scroll": 

				// Add code to index pages.
				if( !is_singular() ) {	

					if (is_search()){

						$nposts = get_option('posts_per_page');

						$se = get_option("imperio_enable_search_everything");

						if ($se == "on"){
							$args = array(
								'showposts' => get_option('posts_per_page'),
								'post_status' => 'publish',
								'paged' => $imperio_pag,
								's' => esc_html($_GET['s'])
							);

						    $imperio_the_query = new WP_Query( $args );

						    $args2 = array(
								'showposts' => -1,
								'post_status' => 'publish',
								'paged' => $imperio_pag,
								's' => esc_html($_GET['s'])
							);

							$counter = new WP_Query($args2);

						} else {
							$args = array(
								'showposts' => get_option('posts_per_page'),
								'post_status' => 'publish',
								'paged' => $imperio_pag,
								'post_type' => 'post',
								's' => esc_html($_GET['s'])
							);

						    $imperio_the_query = new WP_Query( $args );

						    $args2 = array(
								'showposts' => -1,
								'post_status' => 'publish',
								'paged' => $imperio_pag,
								'post_type' => 'post',
								's' => esc_html($_GET['s'])
							);

							$counter = new WP_Query($args2);
						}

						$max = ceil($counter->post_count / $nposts);
						$imperio_pag = 1;

						$imperio_pag = $wp_query->query_vars['paged'];
						if (!is_numeric($imperio_pag)) $imperio_pag = 1;

					} else {
						$max = $wp_query->max_num_pages;
						$imperio_paged = $imperio_pag;

					}


				} else {

					$orderby = "";
					$category = "";

				    $args = array(
	    				'showposts' => $nposts,
	    				'orderby' => $orderby,
	    				'order' => $order,
	    				'cat' => $category,
	    				'post_status' => 'publish'
	    			);

	    		    $imperio_the_query = new WP_Query( $args );


		    		$max = $imperio_the_query->max_num_pages;
		    		$imperio_pag = 1;

					$imperio_pag = $wp_query->query_vars['paged'];
					if (!is_numeric($imperio_pag)) $imperio_pag = 1;		    			    				
	    		}

			break;
	}
	?>
	<div class="imperio_helper_div" id="loader-startPage"><?php echo esc_html($imperio_pag); ?></div>
	<div class="imperio_helper_div" id="loader-maxPages"><?php echo esc_html($max); ?></div>
	<div class="imperio_helper_div" id="loader-nextLink"><?php echo next_posts($max, false); ?></div>
	<div class="imperio_helper_div" id="loader-prevLink"><?php echo previous_posts($max, false); ?></div>
	<?php
}

$imperio_styleColor = "#".esc_html(get_option("imperio_style_color"));
$imperio_bodyLayoutType = get_option("imperio_body_layout_type");
$imperio_headerType = get_option("imperio_header_type");
?>
</div> <!-- END OF MAIN -->
<div id="bodyLayoutType" class="imperio_helper_div"><?php echo esc_html($imperio_bodyLayoutType); ?></div>
<div id="headerType" class="imperio_helper_div"><?php echo esc_html($imperio_headerType); ?></div>
<?php 
	if (get_option("imperio_body_shadow") == "on"){
		?>
			<div id="bodyShadowColor" class="imperio_helper_div"><?php echo "#".esc_html(get_option("imperio_body_shadow_color")); ?></div>
		<?php
	}
	$imperio_headerStyleType = get_option("imperio_header_style_type");
?>
<div id="templatepath" class="imperio_helper_div"><?php echo esc_url(get_template_directory_uri())."/"; ?></div>
<div id="homeURL" class="imperio_helper_div"><?php echo esc_url(home_url("/")); ?></div>
<div id="styleColor" class="imperio_helper_div"><?php echo "#".esc_html(get_option("imperio_style_color")); ?></div>	
<div id="headerStyleType" class="imperio_helper_div"><?php echo esc_html($imperio_headerStyleType); ?></div>
<div class="imperio_helper_div" id="reading_option"><?php 
	if ($imperio_reading_option == "scrollauto"){
		$detect = new Mobile_Detect();
		if ($detect->isMobile())
			$imperio_reading_option = "scroll";
	}
	echo esc_html($imperio_reading_option); 
?></div>
<?php
	$imperio_color_code = get_option("imperio_style_color");
?>
<div class="imperio_helper_div" id="imperio_no_more_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "imperio"), icl_t( 'imperio', 'No more posts to load.', get_option('imperio_no_more_posts_text')));
	} else {
		printf(esc_html__("%s", "imperio"), get_option('imperio_no_more_posts_text'));
	}
?></div>
<div class="imperio_helper_div" id="imperio_load_more_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "imperio"), icl_t( 'imperio', 'Load More Posts', get_option('imperio_load_more_posts_text')));
	} else {
		printf(esc_html__("%s", "imperio"), get_option('imperio_load_more_posts_text'));
	}
?></div>
<div class="imperio_helper_div" id="imperio_loading_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "imperio"), icl_t( 'imperio', 'Loading posts.', get_option('imperio_loading_posts_text')));
	} else {
		printf(esc_html__("%s", "imperio"), get_option('imperio_loading_posts_text'));
	}
?></div>
<div class="imperio_helper_div" id="imperio_links_color_hover"><?php echo esc_html(str_replace("__USE_THEME_MAIN_COLOR__", $imperio_color_code, get_option("imperio_links_color_hover"))); ?></div>
<div class="imperio_helper_div" id="imperio_enable_images_magnifier"><?php echo esc_html(get_option('imperio_enable_images_magnifier')); ?></div>
<div class="imperio_helper_div" id="imperio_thumbnails_hover_option"><?php echo esc_html(get_option('imperio_thumbnails_hover_option')); ?></div>
<div id="homePATH" class="imperio_helper_div"><?php echo ABSPATH; ?></div>
<div class="imperio_helper_div" id="imperio_menu_color">#<?php echo esc_html(get_option("imperio_menu_color")); ?></div>
<div class="imperio_helper_div" id="imperio_fixed_menu"><?php echo esc_html(get_option("imperio_fixed_menu")); ?></div>
<div class="imperio_helper_div" id="imperio_thumbnails_effect"><?php if (get_option("imperio_animate_thumbnails") == "on") echo esc_html(get_option("imperio_thumbnails_effect")); else echo "none"; ?></div>
<div class="imperio_helper_div loadinger">
	<img alt="loading" src="<?php echo esc_url(get_template_directory_uri()). '/images/ajx_loading.gif' ?>">
</div>
<div class="imperio_helper_div" id="permalink_structure"><?php echo esc_html(get_option('permalink_structure')); ?></div>
<div class="imperio_helper_div" id="headerstyle3_menucolor">#<?php echo esc_html(get_option("imperio_menu_color")); ?></div>
<div class="imperio_helper_div" id="disable_responsive_layout"><?php echo esc_html(get_option('imperio_disable_responsive')); ?></div>
<div class="imperio_helper_div" id="filters-dropdown-sort"><?php esc_html_e('Sort Gallery','imperio'); ?></div>
<div class="imperio_helper_div" id="searcheverything"><?php echo esc_html(get_option("imperio_enable_search_everything")); ?></div>
<div class="imperio_helper_div" id="imperio_header_shrink"><?php if (get_option('imperio_fixed_menu') == 'on'){if (get_option('imperio_header_after_scroll') == 'on'){if (get_option('imperio_header_shrink_effect') == 'on'){echo "yes";} else echo "no";}} ?></div>
<div class="imperio_helper_div" id="imperio_header_after_scroll"><?php if (get_option('imperio_fixed_menu') == 'on'){if (get_option('imperio_header_after_scroll') == 'on'){echo "yes";} else echo "no";} ?></div>
<div class="imperio_helper_div" id="imperio_grayscale_effect"><?php echo esc_html(get_option("imperio_enable_grayscale")); ?></div>
<div class="imperio_helper_div" id="imperio_enable_ajax_search"><?php echo esc_html(get_option("imperio_enable_ajax_search")); ?></div>
<div class="imperio_helper_div" id="imperio_menu_add_border"><?php echo esc_html(get_option("imperio_menu_add_border")); ?></div>
<div class="imperio_helper_div" id="imperio_newsletter_input_text"><?php
	if (function_exists('icl_t')){
		echo esc_html(icl_t( 'imperio', 'Enter your email here', get_option('imperio_newsletter_input_text')));
	} else {
		echo esc_html(get_option('imperio_newsletter_input_text'));
	}
?></div>
<div class="imperio_helper_div" id="imperio_content_to_the_top">
	<?php 
		if (is_singular() && get_post_meta(get_the_ID(), 'imperio_enable_custom_header_options_value', true)=='yes') echo esc_html(get_post_meta(get_the_ID(), 'imperio_content_to_the_top_value', true));
		else echo esc_html(get_option('imperio_content_to_the_top')); 
	?>
</div>
<div class="imperio_helper_div" id="imperio_update_section_titles"><?php echo esc_html(get_option('imperio_update_section_titles')); ?></div>
<?php
	if (function_exists('icl_t')){
		?>
		<div class="imperio_helper_div" id="imperio_wpml_current_lang"><?php echo ICL_LANGUAGE_CODE; ?></div>
		<?php
	}
?>
<?php 
	$standardfonts = array('Arial','Arial Black','Helvetica','Helvetica Neue','Courier New','Georgia','Impact','Lucida Sans Unicode','Times New Roman', 'Trebuchet MS','Verdana','');
	$importfonts = "";
	$imperio_import_fonts = imperio_get_import_fonts();

	foreach ($imperio_import_fonts as $font){
		if (!in_array($font,$standardfonts)){
			$font = str_replace(" ","+",str_replace("|",":",$font));
			if ($importfonts=="") $importfonts .= $font;
			else {
				if (strpos($importfonts, $font) === false)
					$importfonts .= "|{$font}";
			}
		}
	}

	if ($importfonts!="") {
		$imperio_import_fonts = $importfonts;
		imperio_set_import_fonts($imperio_import_fonts);
		imperio_google_fonts_scripts();
	}

	if (get_option("imperio_enable_gotop") == "on"){
		?>
		<p id="back-top"><a href="#home"><i class="fa fa-angle-up"></i></a></p>
		<?php
	}
	
	imperio_get_team_profiles_content();
	imperio_get_custom_inline_css();

	if (get_option("imperio_body_type") == "body_boxed"){
		?>
		</div>
		<?php
	}
	
	wp_footer(); 
?>
    	
</body>
</html>