<?php
function imperio_styles(){

	 if (!is_admin()){
		
		wp_enqueue_style('imperio-blog', IMPERIO_CSS_PATH .'blog.css'); 
	 	wp_enqueue_style('imperio-bootstrap', IMPERIO_CSS_PATH .'bootstrap.css');
		wp_enqueue_style('imperio-icons', IMPERIO_CSS_PATH .'icons-font.css');
		wp_enqueue_style('imperio-component', IMPERIO_CSS_PATH .'component.css');
		
		wp_enqueue_style('imperio-IE', IMPERIO_CSS_PATH .'IE.css');	
		wp_style_add_data('imperio-IE','conditional','lt IE 9');
		
		wp_enqueue_style('imperio-editor', get_template_directory_uri().'/editor-style.css');
		wp_enqueue_style('imperio-woo-layout', IMPERIO_CSS_PATH .'imperio-woo-layout.css');
		wp_enqueue_style('imperio-woo', IMPERIO_CSS_PATH .'imperio-woocommerce.css');
		wp_enqueue_style('imperio-ytp', IMPERIO_CSS_PATH .'mb.YTPlayer.css');
		wp_enqueue_style('imperio-retina', IMPERIO_CSS_PATH .'retina.css');
		
	}
}
add_action('wp_enqueue_scripts', 'imperio_styles', 1);

?>