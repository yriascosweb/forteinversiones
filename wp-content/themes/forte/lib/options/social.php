<?php
	
	$imperio_general_options= array( array(
		"name" => "Twitter and Social Icons",
		"type" => "title",
		"img" => IMPERIO_IMAGES_URL."icon_general.png"
	),
	
	( defined('IMPERIO_PLG_ACTIVE') === true ? 
		array(
			"type" => "open",
			"subtitles"=>array(array("id"=>"social", "name"=>"Social Icons"), array("id"=>"twitter", "name" => "Twitter"))
		)
		:
		array(
			"type" => "open",
			"subtitles"=>array(array("id"=>"social", "name"=>"Social Icons"))
		)
	),
	
	/* ------------------------------------------------------------------------*
	 * Top Panel
	 * ------------------------------------------------------------------------*/
	
	array(
		"type" => "subtitle",
		"id"=>'social'
	),
	
	array(
		"type" => "documentation",
		"text" => "<h3>Social Icons</h3>"
	),
	
	array(
		"name" => "Facebook Icon",
		"id" => "imperio_icon-facebook",
		"type" => "text",
		"desc" => "Enter full url   ex: http://facebook.com/UpperThemes",
		"std" => ""
	),
	array(
		"name" => "Twitter Icon",
		"id" => "imperio_icon-twitter",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Tumblr Icon",
		"id" => "imperio_icon-tumblr",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Stumble Upon Icon",
		"id" => "imperio_icon-stumbleupon",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Flickr Icon",
		"id" => "imperio_icon-flickr",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "LinkedIn Icon",
		"id" => "imperio_icon-linkedin",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Delicious Icon",
		"id" => "imperio_icon-delicious",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Skype Icon",
		"id" => "imperio_icon-skype",
		"type" => "text",
		"desc" => "For a directly call to your Skype, add the following code.  skype:username?call",
		"std" => ""
	),
	array(
		"name" => "Digg Icon",
		"id" => "imperio_icon-digg",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Google Icon",
		"id" => "imperio_icon-google-plus",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Vimeo Icon",
		"id" => "imperio_icon-vimeo-square",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "DeviantArt Icon",
		"id" => "imperio_icon-deviantart",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Behance Icon",
		"id" => "imperio_icon-behance",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Instagram Icon",
		"id" => "imperio_icon-instagram",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Blogger Icon",
		"id" => "imperio_icon-blogger",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Wordpress Icon",
		"id" => "imperio_icon-wordpress",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Youtube Icon",
		"id" => "imperio_icon-youtube",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Reddit Icon",
		"id" => "imperio_icon-reddit",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "RSS Icon",
		"id" => "imperio_icon-rss",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "SoundCloud Icon",
		"id" => "imperio_icon-soundcloud",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Pinterest Icon",
		"id" => "imperio_icon-pinterest",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Dribbble Icon",
		"id" => "imperio_icon-dribbble",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "VK Icon",
		"id" => "imperio_icon-vk",
		"type" => "text",
		"std" => ""
	),
	
	array(
		"name" => "Yelp Icon",
		"id" => "imperio_icon-yelp",
		"type" => "text",
		"std" => ""
	),
	
	array(
		"name" => "Twitch Icon",
		"id" => "imperio_icon-twitch",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Houzz Icon",
		"id" => "imperio_icon-houzz",
		"type" => "text",
		"std" => ""
	),
	
	array(
		"name" => "Foursquare Icon",
		"id" => "imperio_icon-foursquare",
		"type" => "text",
		"std" => ""
	),
	
	array(
		"name" => "Slack Icon",
		"id" => "imperio_icon-slack",
		"type" => "text",
		"std" => ""
	),
	
	array(
		"name" => "Whatsapp Icon",
		"id" => "imperio_icon-whatsapp",
		"type" => "text",
		"std" => ""
	),

	array(
		"type" => "close"
	),
	
	/* twitter options */ 
	array(
		"type" => "subtitle",
		"id"=>'twitter'
	),
	
	array(
		"type" => "documentation",
		"text" => "<h3>Twitter Scroller</h3>"
	),
	
	array(
		"name" => "Twitter Username",
		"id" => "imperio_twitter_username",
		"type" => "text",
		"std" => ''
	),
	
	array(
		"name" => "Twitter App Consumer Key",
		"id" => "twitter_consumer_key",
		"type" => "text"
	),
	
	array(
		"name" => "Twitter App Consumer Secret",
		"id" => "twitter_consumer_secret",
		"type" => "text"
	),
	
	array(
		"name" => "Twitter App Access Token",
		"id" => "twitter_user_token",
		"type" => "text"
	),
	
	array(
		"name" => "Twitter App Access Token Secret",
		"id" => "twitter_user_secret",
		"type" => "text"
	),
	
	array(
		"name" => "Number Tweets",
		"id" => "imperio_twitter_number_tweets",
		"type" => "text",
		"std" => '5'
	),
	
	array( "type" => "close" ),
	
		
	/*close array*/
	
	array(
		"type" => "close"
	));
	
	imperio_add_options($imperio_general_options);
	
?>