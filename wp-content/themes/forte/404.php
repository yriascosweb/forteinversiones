<?php get_header(); imperio_print_menu(); $imperio_color_code = get_option("imperio_style_color"); ?>

	<div class="container">
		<div class="entry-header">
			<div class="error-c">
				<img src="<?php echo esc_url(get_template_directory_uri() . "/images/error.png");?>" title=""/>
				<br>
				<h1 class="heading-error"><?php
					if (function_exists('icl_t')){
						wp_kses_post(printf(esc_html__( "%s", "imperio" ), stripslashes_from_strings_only(icl_t( 'imperio', 'Oops! There is nothing here...', get_option('imperio_404_heading')))));
					} else {
						wp_kses_post(printf(esc_html__( "%s", "imperio" ), stripslashes_from_strings_only(get_option('imperio_404_heading'))));
					}
				?></h1>
							
				<p class="text-error"><?php
					if (function_exists('icl_t')){
						wp_kses_post(printf(esc_html__( "%s", "imperio" ), stripslashes_from_strings_only(icl_t( 'imperio', "It seems we can't find what you're looking for. Perhaps searching one of the links in the above menu, can help.", get_option('imperio_404_text')))));
					} else {
						wp_kses_post(printf(esc_html__( "%s", "imperio" ), stripslashes_from_strings_only(get_option('imperio_404_text'))));
					}
				?></p>
				
				<a href="<?php echo esc_url(home_url("/")); ?>" class="errorbutton"><?php
					if (function_exists('icl_t')){
						printf(esc_html__("%s","imperio"), icl_t( 'imperio', 'GO TO HOMEPAGE', get_option('imperio_404_button_text')));
					} else {
						printf(esc_html__("%s","imperio"), get_option('imperio_404_button_text'));
					}
				?></a>
			</div>
			
		</div>
	</div>
<?php get_footer(); ?>