<?php
/*
Template Name: Blank Template (No header nor footer)
*/
get_header(); //the_post();
$theid = (isset($imperio_uc_id)) ? $imperio_uc_id : get_the_ID();
$post = get_post($theid);
if (class_exists('Ultimate_VC_Addons')) {
	if(stripos($post->post_content, 'font_call:')){
		preg_match_all('/font_call:(.*?)"/',$post->post_content, $display);
		enquque_ultimate_google_fonts_optimzed($display[1]);
	}
	imperio_google_fonts_scripts();
}
?>
<div class="page-template page-template-template-blank page-template-template-blank-php <?php echo esc_attr("the-id-is-$theid"); ?>">
	<div class="fullwindow_content container">
		<div class="tb-row">
			<div class="tb-cell">
				<?php 
					if (post_password_required()) echo get_the_password_form();
					else {
						if (is_preview()){
							wp_reset_postdata();
							the_content();
						} else {
							if (function_exists('wpb_js_remove_wpautop') == true)
								echo wpb_js_remove_wpautop($post->post_content);
							else echo wp_kses_post($post->post_content); 
							/* custom element css */
							$shortcodes_custom_css = get_post_meta( $theid, '_wpb_shortcodes_custom_css', true );
							if ( ! empty( $shortcodes_custom_css ) ) {
								imperio_set_custom_inline_css($shortcodes_custom_css);
							}
						}
					}
				?>
			</div>
		</div>
	</div>
	<?php wp_footer(); ?>
<div id="templatepath" class="imperio_helper_div"><?php  echo get_template_directory_uri()."/"; ?></div>
<div id="homeURL" class="imperio_helper_div"><?php echo esc_url(home_url('/')); ?>/</div>
<div id="homePATH" class="imperio_helper_div"><?php echo ABSPATH; ?></div>
<div id="styleColor" class="imperio_helper_div"><?php $imperio_styleColor = "#".get_option("imperio"."_style_color"); echo esc_html($imperio_styleColor);?></div>	
<div id="headerStyleType" class="imperio_helper_div"><?php $imperio_headerStyleType = get_option("imperio"."_header_style_type"); echo esc_html($imperio_headerStyleType); ?></div>
<div class="imperio_helper_div" id="reading_option"><?php 
	$imperio_reading_option = get_option('imperio_blog_reading_type');
	if ($imperio_reading_option == "scrollauto"){
		$detect = new imperio_Mobile_Detect();
		if ($detect->isMobile())
			$imperio_reading_option = "scroll";
	}
	echo esc_html($imperio_reading_option); 
?></div>
<?php
	$imperio_color_code = get_option("imperio_style_color");
?>
<div class="imperio_helper_div" id="imperio_no_more_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "imperio"), icl_t( 'imperio', 'No more posts to load.', get_option('imperio_no_more_posts_text')));
	} else {
		printf(esc_html__("%s", "imperio"), get_option('imperio_no_more_posts_text'));
	}
?></div>
<div class="imperio_helper_div" id="imperio_load_more_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "imperio"), icl_t( 'imperio', 'Load More Posts', get_option('imperio_load_more_posts_text')));
	} else {
		printf(esc_html__("%s", "imperio"), get_option('imperio_load_more_posts_text'));
	}
?></div>
<div class="imperio_helper_div" id="imperio_loading_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "imperio"), icl_t( 'imperio', 'Loading posts.', get_option('imperio_loading_posts_text')));
	} else {
		printf(esc_html__("%s", "imperio"), get_option('imperio_loading_posts_text'));
	}
?></div><div class="imperio_helper_div" id="imperio_links_color_hover"><?php echo str_replace("__USE_THEME_MAIN_COLOR__", $imperio_color_code, get_option("imperio_links_color_hover")); ?></div>
<div class="imperio_helper_div" id="imperio_enable_images_magnifier"><?php echo get_option('imperio_enable_images_magnifier'); ?></div>
<div class="imperio_helper_div" id="imperio_thumbnails_hover_option"><?php echo get_option('imperio_thumbnails_hover_option'); ?></div>
<div class="imperio_helper_div" id="imperio_menu_color">#<?php echo get_option("imperio"."_menu_color"); ?></div>
<div class="imperio_helper_div" id="imperio_fixed_menu"><?php echo get_option("imperio"."_fixed_menu"); ?></div>
<div class="imperio_helper_div" id="imperio_thumbnails_effect"><?php if (get_option("imperio"."_animate_thumbnails") == "on") echo get_option("imperio_thumbnails_effect"); else echo "none"; ?></div>
<div class="imperio_helper_div" id="permalink_structure"><?php echo get_option('permalink_structure'); ?></div>
<div class="imperio_helper_div" id="headerstyle3_menucolor">#<?php echo get_option("imperio"."_menu_color"); ?></div>
<div class="imperio_helper_div" id="disable_responsive_layout"><?php echo get_option('imperio_disable_responsive'); ?></div>
<div class="imperio_helper_div" id="filters-dropdown-sort"><?php esc_html_e('Sort Gallery','imperio'); ?></div>
<div class="imperio_helper_div" id="searcheverything"><?php echo get_option("imperio"."_enable_search_everything"); ?></div>
<div class="imperio_helper_div" id="imperio_header_shrink"><?php if (get_option('imperio_fixed_menu') == 'on'){if (get_option('imperio_header_after_scroll') == 'on'){if (get_option('imperio_header_shrink_effect') == 'on'){echo "yes";} else echo "no";}} ?></div>
<div class="imperio_helper_div" id="imperio_header_after_scroll"><?php if (get_option('imperio_fixed_menu') == 'on'){if (get_option('imperio_header_after_scroll') == 'on'){echo "yes";} else echo "no";} ?></div>
<div class="imperio_helper_div" id="imperio_grayscale_effect"><?php echo get_option("imperio"."_enable_grayscale"); ?></div>
<div class="imperio_helper_div" id="imperio_enable_ajax_search"><?php echo get_option("imperio"."_enable_ajax_search"); ?></div>
<div class="imperio_helper_div" id="imperio_menu_add_border"><?php echo get_option("imperio"."_menu_add_border"); ?></div>
<div class="imperio_helper_div" id="imperio_newsletter_input_text"><?php
	if (function_exists('icl_t')){
		echo esc_html(icl_t( 'imperio', 'Enter your email here', get_option('imperio_newsletter_input_text')));
	} else {
		echo esc_html(get_option('imperio_newsletter_input_text'));
	}
?></div>
<?php
	if (function_exists('icl_t')){
		?>
		<div class="imperio_helper_div" id="imperio_wpml_current_lang"><?php echo ICL_LANGUAGE_CODE; ?></div>
		<?php
	}
?>
</div>
