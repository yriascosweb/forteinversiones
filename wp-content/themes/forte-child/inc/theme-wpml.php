<?php

//LANGUAGES TAB
icl_register_string( 'imperio', 'Home', get_option('imperio_breadcrumbs_home_text'));
icl_register_string( 'imperio', 'You are here:', get_option('imperio_you_are_here'));
icl_register_string( 'imperio', 'Latest from <span class=text_color>Twitter</span>', get_option('imperio_latest_for_twitter'));
icl_register_string( 'imperio', 'Open Menu', get_option('imperio_open_menu'));

icl_register_string( 'imperio', 'Next Project', get_option('imperio_next_single_proj'));
icl_register_string( 'imperio', 'Previous Project', get_option('imperio_prev_single_proj'));
icl_register_string( 'imperio', 'SHARE THIS PROJECT', get_option('imperio_share_proj_text'));

icl_register_string( 'imperio', 'read more', get_option('imperio_read_more'));
icl_register_string( 'imperio', 'Previous posts', get_option('imperio_previous_text'));
icl_register_string( 'imperio', 'Next posts', get_option('imperio_next_text'));
icl_register_string( 'imperio', 'Previous post', get_option('imperio_single_previous_text'));
icl_register_string( 'imperio', 'Next post', get_option('imperio_single_next_text'));
icl_register_string( 'imperio', 'by', get_option('imperio_by_text'));
icl_register_string( 'imperio', 'in', get_option('imperio_in_text'));
icl_register_string( 'imperio', 'Tags', get_option('imperio_tags_text'));
icl_register_string( 'imperio', 'Load More Posts', get_option('imperio_load_more_posts_text'));
icl_register_string( 'imperio', 'No more posts to load.', get_option('imperio_no_more_posts_text'));
icl_register_string( 'imperio', 'Loading posts.', get_option('imperio_loading_posts_text'));
icl_register_string( 'imperio', 'SHARE THIS', get_option('imperio_share_post_text'));

icl_register_string( 'imperio', 'No comments', get_option('imperio_no_comments_text'));
icl_register_string( 'imperio', 'comment', get_option('imperio_comment_text'));
icl_register_string( 'imperio', 'comments', get_option('imperio_comments_text'));

icl_register_string( 'imperio', 'Type your search and hit enter...', get_option('imperio_search_box_text'));
icl_register_string( 'imperio', 'Search results for', get_option('imperio_search_results_text'));
icl_register_string( 'imperio', 'No results found.', get_option('imperio_no_results_text'));
icl_register_string( 'imperio', 'Next results', get_option('imperio_next_results'));
icl_register_string( 'imperio', 'Previous results', get_option('imperio_previous_results'));

icl_register_string( 'imperio', 'Name', get_option('imperio_cf_name'));
icl_register_string( 'imperio', 'Email', get_option('imperio_cf_email'));
icl_register_string( 'imperio', 'Message', get_option('imperio_cf_message'));
icl_register_string( 'imperio', 'Send', get_option('imperio_cf_send'));

icl_register_string( 'imperio', 'Follow us on Twitter', get_option('imperio_twitter_follow_us'));
icl_register_string( 'imperio', 'I was looking at ', get_option('imperio_twitter_pre_tweet'));

icl_register_string( 'imperio', 'Oops! There is nothing here...', get_option('imperio_404_heading'));
icl_register_string( 'imperio', "It seems we can't find what you're looking for. Perhaps searching one of the links in the above menu, can help.", get_option('imperio_404_text'));
icl_register_string( 'imperio', 'GO TO HOMEPAGE', get_option('imperio_404_button_text'));

//NEWSLETTER TAB
icl_register_string( 'imperio', 'Subscribe our <span class=text_color>Newsletter</span>', get_option('imperio_newsletter_text'));
icl_register_string( 'imperio', 'Subscribe to our newsletter to receive news, cool free stuff updates and new released products (no spam!)', get_option('imperio_newsletter_stext'));
icl_register_string( 'imperio', 'Enter your email here', get_option('imperio_newsletter_input_text'));

?>
