<?php
	
	$imperio_fonts_array = imperio_fonts_array_builder();
	
	$imperio_style_general_options= array( array(
		"name" => "Website Loading",
		"type" => "title"
	),
	
	array(
		"type" => "open",
		"subtitles"=>array(array("id"=>"website_loading", "name"=>"Website Loading"))
	),
	
	/* ------------------------------------------------------------------------*
	 * WEBSITE LOADING
	 * ------------------------------------------------------------------------*/
	array(
		"type" => "subtitle",
		"id" => 'website_loading'
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Website Loading</h3>'
	),
	
	array(
		"name" => "Background Color",
		"id" => "imperio_loader_background",
		"type" => "color",
		"std" => "ffffff"
	),
	
	array(
		"name" => "Loader Color",
		"id" => "imperio_loader_color",
		"type" => "color",
		"std" => "7dc771"
	),
	
	array(
		"name" => "Percentage Font",
		"id" => "imperio_loader_percentage_font",
		"type" => "select",
		"options" => $imperio_fonts_array,
		"desc" => 'You can select one of the fonts that the theme goes with or you can add google fonts (Style Options > Fonts).',
		"std" => "Helvetica Neue"
	),
	
	array(
		"name" => "Percentage Font Size",
		"id" => "imperio_loader_percentage_font_size",
		"type" => "slider",
		"std" => "16px",
	),
	
	array(
		"name" => "Percentage Font Color",
		"id" => "imperio_loader_percentage_font_color",
		"type" => "color",
		"std" => "ffffff"
	),
	
	array(
		"type" => "close"
	),
	
	array(
		"type" => "close"
	));
	
	imperio_add_style_options($imperio_style_general_options);
	
?>