<?php

if (!defined('IMPERIO_PLG_URL')) die('No shilly shally, kids.');

add_action( 'upper_vc_load_default_templates_action','upper_studio_templates_for_vc' ); 

function upper_studio_templates_for_vc() {

$imperio_get_directory = IMPERIO_PLG_URL."lib/upper-studio/";

$cat_display_names = array(	
	'about' => __('About', 'imperio'),
	'counters' => __('Counters', 'imperio'),
	'faq' => __('FAQ Section', 'imperio'),
	'general' => __('General', 'imperio'),
	'icons' => __('Icons', 'imperio'),
	'contactforms' => __('Contact Forms', 'imperio'),
	'portfolio' => __('Portfolio', 'imperio'),
	'pricing' => __('Pricing', 'imperio'),
	'services' => __('Services', 'imperio'),'team' => __('Team', 'imperio'),
	'verticalstabsicon' => __('Stylish Tabs','imperio'),
	'testimonials' => __('Testimonials', 'imperio'),
	'team' => __('Team', 'imperio')
);

}

?>