<?php 
if ( ! defined( 'ABSPATH' ) ) {
	die;
	} // Cannot access pages directly.
/**
 *
 * ------------------------------------------------------------------------------------------------
 * Text Domains: sp-framework
 * ------------------------------------------------------------------------------------------------
 *
 */

// ------------------------------------------------------------------------------------------------
require_once plugin_dir_path( __FILE__ ) . '/sp-framework-path.php';
// ------------------------------------------------------------------------------------------------

if ( ! function_exists( 'wpl_wcs_framework_init' ) && ! class_exists( 'WPL_WCS_Framework' ) ) {
	function wpl_wcs_framework_init() {

		// active modules.
		defined( 'WPL_WCS_F_ACTIVE_METABOX' ) or define( 'WPL_WCS_F_ACTIVE_METABOX', true );
		defined( 'WPL_WCS_F_ACTIVE_FRAMEWORK' ) or define( 'WPL_WCS_F_ACTIVE_FRAMEWORK', true );

		// helpers.
		wpl_wcs_locate_template( 'functions/fallback.php' );
		wpl_wcs_locate_template( 'functions/helpers.php' );
		wpl_wcs_locate_template( 'functions/actions.php' );
		wpl_wcs_locate_template( 'functions/enqueue.php' );
		wpl_wcs_locate_template( 'functions/sanitize.php' );
		wpl_wcs_locate_template( 'functions/validate.php' );

		// classes.
		wpl_wcs_locate_template( 'classes/abstract.class.php' );
		wpl_wcs_locate_template( 'classes/options.class.php' );
		wpl_wcs_locate_template( 'classes/metabox.class.php' );
		wpl_wcs_locate_template( 'classes/framework.class.php' );

		// configs.
		wpl_wcs_locate_template( 'config/metabox.config.php' );
	}

	add_action( 'init', 'wpl_wcs_framework_init', 10 );

}
