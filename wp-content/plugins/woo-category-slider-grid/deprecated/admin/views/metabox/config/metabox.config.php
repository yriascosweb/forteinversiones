<?php if ( ! defined( 'ABSPATH' ) ) {
	die;
}
// Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// METABOX OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options = array();

// -----------------------------------------
// Shortcode Generator Options
// -----------------------------------------
$options[] = array(
	'id'        => 'wpl_wcs_shortcode_options',
	'title'     => __( 'Shortcode Options', 'woo-category-slider' ),
	'post_type' => 'wpl_wcslider',
	'context'   => 'normal',
	'priority'  => 'default',
	'sections'  => array(

		// begin: a section.
		array(
			'name'   => 'wpl_wcs_shortcode_option_1',
			'title'  => __( 'General Settings', 'woo-category-slider' ),
			'icon'   => 'fa fa-wrench',

			// begin: fields.
			'fields' => array(

				// begin: a field.
				array(
					'id'      => 'layout',
					'type'    => 'select',
					'title'   => __( 'Layout', 'woo-category-slider' ),
					'desc'    => __( 'Select which layout you want to display', 'woo-category-slider' ),
					'options' => array(
						'slider'         => __( 'Slider', 'woo-category-slider' ),
						'grid'       => __( 'Grid', 'woo-category-slider' ),
					),
					'default' => 'slider',
					'class'   => 'chosen',
				),
				array(
					'id'      => 'theme',
					'type'    => 'select',
					'title'   => __( 'Choose a Theme', 'woo-category-slider' ),
					'desc'    => __( 'Select which theme you want to display.', 'woo-category-slider' ),
					'options' => array(
						'theme_one'       => __( 'Theme One', 'woo-category-slider' ),
						'theme_two'       => __( 'Theme Two', 'woo-category-slider' ),
						'theme_three'     => __( 'Theme Three', 'woo-category-slider' ),
						'theme_four'      => __( 'Theme Four', 'woo-category-slider' ),
					),
					'default' => 'theme_one',
					'class'   => 'chosen',
				),
				array(
					'id'      => 'display_categories',
					'type'    => 'select',
					'title'   => __( 'Display Categories', 'woo-category-slider' ),
					'desc'    => __( 'Select an option to display the categories.', 'woo-category-slider' ),
					'options' => array(
						'all_cat'      => __( 'All Categories', 'woo-category-slider' ),
						'specific_cat' => __( 'Specific Category', 'woo-category-slider' ),
					),
					'default' => 'all_cat',
					'class'   => 'chosen',
				),
				array(
					'id'         => 'cat_list',
					'type'       => 'select',
					'title'      => __( 'Choose Category(s)', 'woo-category-slider' ),
					'desc'       => __( 'Choose the specific category(s) to show.', 'woo-category-slider' ),
					'options'    => 'wpl_wcs_categories',
					'attributes' => array(
						'multiple'    => 'multiple',
						'placeholder' => __( 'Select Category(s)', 'woo-category-slider' ),
						'style'       => 'width: 280px;',
					),
					'class'      => 'chosen',
					'dependency' => array( 'display_categories', '==', 'specific_cat' ),
				),
				array(
					'id'      => 'hide_empty_cat',
					'type'    => 'checkbox',
					'title'   => __( 'Hide Empty Categories', 'woo-category-slider' ),
					'desc'    => __( 'Check to hide empty categories from the slider.', 'woo-category-slider' ),
				),
				array(
					'id'      => 'include_child_cat',
					'type'    => 'checkbox',
					'title'   => __( 'Include Child Categories', 'woo-category-slider' ),
					'desc'    => __( 'Check to include child categories from the slider.', 'woo-category-slider' ),
				),
				array(
					'id'      => 'number_of_total_cat',
					'type'    => 'number',
					'title'   => __( 'Total Categories', 'woo-category-slider' ),
					'desc'    => __( 'Limit the number of total categories to display.', 'woo-category-slider' ),
					'default' => '50',
				),
				array(
					'id'         => 'number_of_cat',
					'type'       => 'number',
					'title'      => __( 'Category Column(s)', 'woo-category-slider' ),
					'desc'       => __( 'Set number of column(s) for the screen larger than 1280px.', 'woo-category-slider' ),
					'default'    => '4',

				),
				array(
					'id'         => 'number_of_cat_desktop',
					'type'       => 'number',
					'title'      => __( 'Category Column(s) on Desktop', 'woo-category-slider' ),
					'desc'       => __( 'Set number of column on desktop for the screen smaller than 1280px.', 'woo-category-slider' ),
					'default'    => '3',

				),
				array(
					'id'         => 'number_of_cat_small_desktop',
					'type'       => 'number',
					'title'      => __( 'Category Column(s) on Small Desktop', 'woo-category-slider' ),
					'desc'       => __( 'Set number of column on small desktop for the screen smaller than 980px.', 'woo-category-slider' ),
					'default'    => '2',

				),
				array(
					'id'         => 'number_of_cat_tablet',
					'type'       => 'number',
					'title'      => __( 'Category Column(s) on Tablet', 'woo-category-slider' ),
					'desc'       => __( 'Set number of column on tablet for the screen smaller than 736px.', 'woo-category-slider' ),
					'default'    => '2',

				),
				array(
					'id'         => 'number_of_cat_mobile',
					'type'       => 'number',
					'title'      => __( 'Category Column(s) on Mobile', 'woo-category-slider' ),
					'desc'       => __( 'Set number of column on mobile for the screen smaller than 480px.', 'woo-category-slider' ),
					'default'    => '1',

				),
				array(
					'id'      => 'cat_order_by',
					'type'    => 'select',
					'title'   => __( 'Order by', 'woo-category-slider' ),
					'desc'    => __( 'Select an order by option.', 'woo-category-slider' ),
					'options' => array(
						'ID'         => __( 'ID', 'woo-category-slider' ),
						'date'       => __( 'Date', 'woo-category-slider' ),
						'count'      => __( 'Count', 'woo-category-slider' ),
						'name'       => __( 'Name', 'woo-category-slider' ),
						'modified'   => __( 'Modified', 'woo-category-slider' ),
						'none'       => __( 'None', 'woo-category-slider' ),
					),
					'default' => 'date',
					'class'   => 'chosen',
				),
				array(
					'id'      => 'cat_order',
					'type'    => 'select',
					'title'   => __( 'Order', 'woo-category-slider' ),
					'desc'    => __( 'Select an order option.', 'woo-category-slider' ),
					'options' => array(
						'ASC'  => __( 'Ascending', 'woo-category-slider' ),
						'DESC' => __( 'Descending', 'woo-category-slider' ),
					),
					'default' => 'DESC',
					'class'   => 'chosen',
				),


			), // end: fields.
		), // end: General section.

		// begin: a section.
		array(
			'name'   => 'wpl_wcs_shortcode_option_2',
			'title'  => __( 'Slider Settings', 'woo-category-slider' ),
			'icon'   => 'fa fa-sliders',

			// begin: fields.
			'fields' => array(

				// begin: a field.
				array(
					'type'       => 'notice',
					'class'      => 'info',
					'content'    => __( 'To see the slider setting fields, select Slider layout.', 'woo-category-slider' ),
					'dependency' => array( 'layout', '!=', 'slider' ),
				),
				array(
					'id'      => 'auto_play',
					'type'    => 'switcher',
					'title'   => __( 'AutoPlay', 'woo-category-slider' ),
					'desc'    => __( 'On/Off auto play.', 'woo-category-slider' ),
					'default' => true,
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'      => 'auto_play_speed',
					'type'    => 'number',
					'title'   => __( 'AutoPlay Speed', 'woo-category-slider' ),
					'desc'    => __( 'Set auto play speed.', 'woo-category-slider' ),
					'default' => '3000',
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'      => 'slider_speed',
					'type'    => 'number',
					'title'   => __( 'Slider Speed', 'woo-category-slider' ),
					'desc'    => __( 'Set slide scroll speed.', 'woo-category-slider' ),
					'default' => '600',
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'      => 'pause_on_hover',
					'type'    => 'switcher',
					'title'   => __( 'Pause on Hover', 'woo-category-slider' ),
					'desc'    => __( 'On/Off pause on hover.', 'woo-category-slider' ),
					'default' => true,
					'dependency' => array( 'layout', '==', 'slider' ),
				),

				array(
					'type'       => 'subheading',
					'content'    => __( 'Navigation Settings', 'woo-category-slider' ),
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'      => 'navigation',
					'type'    => 'switcher',
					'title'   => __( 'Navigation Arrow', 'woo-category-slider' ),
					'desc'    => __( 'Show/Hide navigation arrow.', 'woo-category-slider' ),
					'default' => true,
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'         => 'navigation_bg',
					'type'       => 'color_picker',
					'title'      => __( 'Background', 'woo-category-slider' ),
					'desc'       => __( 'Set navigation background color.', 'woo-category-slider' ),
					'default'    => 'transparent',
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'         => 'navigation_color',
					'type'       => 'color_picker',
					'title'      => __( 'Color', 'woo-category-slider' ),
					'desc'       => __( 'Set navigation icon color.', 'woo-category-slider' ),
					'default'    => '#aaaaaa',
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'         => 'navigation_border_color',
					'type'       => 'color_picker',
					'title'      => __( 'Border Color', 'woo-category-slider' ),
					'desc'       => __( 'Set navigation border color.', 'woo-category-slider' ),
					'default'    => '#aaaaaa',
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'         => 'navigation_hover_bg',
					'type'       => 'color_picker',
					'title'      => __( 'Hover Background', 'woo-category-slider' ),
					'desc'       => __( 'Set navigation hover background color.', 'woo-category-slider' ),
					'default'    => '#cc2b5e',
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'         => 'navigation_hover_color',
					'type'       => 'color_picker',
					'title'      => __( 'Hover Color', 'woo-category-slider' ),
					'desc'       => __( 'Set navigation hover icon color.', 'woo-category-slider' ),
					'default'    => '#ffffff',
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'         => 'navigation_hover_border_color',
					'type'       => 'color_picker',
					'title'      => __( 'Hover Border Color', 'woo-category-slider' ),
					'desc'       => __( 'Set navigation hover border color.', 'woo-category-slider' ),
					'default'    => '#cc2b5e',
					'dependency' => array( 'layout', '==', 'slider' ),
				),

				array(
					'type'       => 'subheading',
					'content'    => __( 'Pagination Settings', 'woo-category-slider' ),
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'      => 'pagination',
					'type'    => 'switcher',
					'title'   => __( 'Pagination', 'woo-category-slider' ),
					'desc'    => __( 'Show/Hide pagination.', 'woo-category-slider' ),
					'default' => true,
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'         => 'pagination_bg',
					'type'       => 'color_picker',
					'title'      => __( 'Background', 'woo-category-slider' ),
					'desc'       => __( 'Set pagination background color.', 'woo-category-slider' ),
					'default'    => '#cccccc',
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'         => 'pagination_active_bg',
					'type'       => 'color_picker',
					'title'      => __( 'Active Background', 'woo-category-slider' ),
					'desc'       => __( 'Set pagination active background color.', 'woo-category-slider' ),
					'default'    => '#cc2b5e',
					'dependency' => array( 'layout', '==', 'slider' ),
				),

				array(
					'type'       => 'subheading',
					'content'    => __( 'Misc Settings', 'woo-category-slider' ),
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'      => 'swipe',
					'type'    => 'switcher',
					'title'   => __( 'Swipe', 'woo-category-slider' ),
					'desc'    => __( 'On/Off swipe mode.', 'woo-category-slider' ),
					'default' => true,
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'      => 'mouse_draggable',
					'type'    => 'switcher',
					'title'   => __( 'Mouse Draggable', 'woo-category-slider' ),
					'desc'    => __( 'On/Off mouse draggable mode.', 'woo-category-slider' ),
					'default' => true,
					'dependency' => array( 'layout', '==', 'slider' ),
				),
				array(
					'id'      => 'rtl',
					'type'    => 'switcher',
					'title'   => __( 'RTL', 'woo-category-slider' ),
					'desc'    => __( 'On/Off RTL mode.', 'woo-category-slider' ),
					'default' => false,
					'dependency' => array( 'layout', '==', 'slider' ),
				),


			), // end: fields.
		), // end: General section.

		// begin: Stylization section.
		array(
			'name'   => 'wpl_wcs_shortcode_option_3',
			'title'  => __( 'Stylization', 'woo-category-slider' ),
			'icon'   => 'fa fa-paint-brush',
			'fields' => array(

				array(
					'id'      => 'slider_title',
					'type'    => 'switcher',
					'title'   => __( 'Slider Section Title', 'woo-category-slider' ),
					'desc'    => __( 'Show/Hide slider section title.', 'woo-category-slider' ),
					'default' => true,
				),
				array(
					'id'         => 'slider_title_color',
					'type'       => 'color_picker',
					'title'      => __( 'Slider Section Title Color', 'woo-category-slider' ),
					'desc'       => __( 'Set slider section title color.', 'woo-category-slider' ),
					'default'    => '#444444',
					'dependency' => array( 'slider_title', '==', 'true' ),
				),

				array(
					'type'       => 'subheading',
					'content'    => __( 'Category Info', 'woo-category-slider' ),
				),
				array(
					'id'         => 'category_info_bg',
					'type'       => 'color_picker',
					'title'      => __( 'Category Info Background', 'woo-category-slider' ),
					'desc'       => __( 'Set category info background color.', 'woo-category-slider' ),
					'default'    => '#eeeeee',
					'dependency' => array( 'theme', 'any', 'theme_three,theme_two' ),
				),
				array(
					'id'         => 'category_info_overlay',
					'type'       => 'color_picker',
					'title'      => __( 'Category Info Overlay', 'woo-category-slider' ),
					'desc'       => __( 'Set category info overlay color.', 'woo-category-slider' ),
					'default'    => 'rgba(17, 17, 17, 0.5)',
					'dependency' => array( 'theme', 'any', 'theme_four,theme_one' ),
				),
				array(
					'id'      => 'cat_name',
					'type'    => 'switcher',
					'title'   => __( 'Category Name', 'woo-category-slider' ),
					'desc'    => __( 'Show/Hide category name.', 'woo-category-slider' ),
					'default' => true,
				),
				array(
					'id'         => 'cat_name_color',
					'type'       => 'color_picker',
					'title'      => __( 'Category Name Color', 'woo-category-slider' ),
					'desc'       => __( 'Set category name color.', 'woo-category-slider' ),
					'default'    => '#444444',
					'dependency' => array( 'cat_name|theme', '==|any', 'true|theme_three,theme_two' ),
				),
				array(
					'id'         => 'cat_name_color2',
					'type'       => 'color_picker',
					'title'      => __( 'Category Name Color', 'woo-category-slider' ),
					'desc'       => __( 'Set category name color.', 'woo-category-slider' ),
					'default'    => '#ffffff',
					'dependency' => array( 'cat_name|theme', '==|any', 'true|theme_four,theme_one' ),
				),
				array(
					'id'      => 'product_count',
					'type'    => 'switcher',
					'title'   => __( 'Product Count', 'woo-category-slider' ),
					'desc'    => __( 'Show/Hide product count.', 'woo-category-slider' ),
					'default' => true,
				),
				array(
					'id'         => 'product_count_color',
					'type'       => 'color_picker',
					'title'      => __( 'Product Count Color', 'woo-category-slider' ),
					'desc'       => __( 'Set product count color.', 'woo-category-slider' ),
					'default'    => '#444444',
					'dependency' => array( 'product_count|theme', '==|any', 'true|theme_three' ),
				),
				array(
					'id'         => 'product_count_color2',
					'type'       => 'color_picker',
					'title'      => __( 'Product Count Color', 'woo-category-slider' ),
					'desc'       => __( 'Set product count color.', 'woo-category-slider' ),
					'default'    => '#ffffff',
					'dependency' => array( 'product_count|theme', '==|any', 'true|theme_four' ),
				),
				array(
					'id'      => 'shop_now',
					'type'    => 'switcher',
					'title'   => __( 'Shop Now Button', 'woo-category-slider' ),
					'desc'    => __( 'Show/Hide shop now button.', 'woo-category-slider' ),
					'default' => true,
					'dependency' => array( 'theme', 'any', 'theme_three,theme_four' ),
				),
				array(
					'id'         => 'shop_now_bg',
					'type'       => 'color_picker',
					'title'      => __( 'Shop Now Button Background', 'woo-category-slider' ),
					'desc'       => __( 'Set shop now button background color.', 'woo-category-slider' ),
					'default'    => '#cc2b5e',
					'dependency' => array( 'shop_now|theme', '==|any', 'true|theme_three,theme_four' ),
				),
				array(
					'id'         => 'shop_now_color',
					'type'       => 'color_picker',
					'title'      => __( 'Shop Now Button Color', 'woo-category-slider' ),
					'desc'       => __( 'Set shop now button text color.', 'woo-category-slider' ),
					'default'    => '#ffffff',
					'dependency' => array( 'shop_now|theme', '==|any', 'true|theme_three,theme_four' ),
				),
				array(
					'id'         => 'shop_now_hover_bg',
					'type'       => 'color_picker',
					'title'      => __( 'Shop Now Button Hover Background', 'woo-category-slider' ),
					'desc'       => __( 'Set shop now button hover background color.', 'woo-category-slider' ),
					'default'    => '#b92554',
					'dependency' => array( 'shop_now|theme', '==|any', 'true|theme_three,theme_four' ),
				),
				array(
					'id'         => 'shop_now_hover_color',
					'type'       => 'color_picker',
					'title'      => __( 'Shop Now Button Hover Color', 'woo-category-slider' ),
					'desc'       => __( 'Set shop now button hover text color.', 'woo-category-slider' ),
					'default'    => '#ffffff',
					'dependency' => array( 'shop_now|theme', '==|any', 'true|theme_three,theme_four' ),
				),
				array(
					'id'      => 'cat_image',
					'type'    => 'switcher',
					'title'   => __( 'Category Image', 'woo-category-slider' ),
					'desc'    => __( 'Show/Hide category image.', 'woo-category-slider' ),
					'default' => true,
					'dependency' => array( 'theme', '!=', 'theme_two' ),
				),


			), // End Fields.
		), // end a section.

	),
);

WPL_WCS_Framework_Metabox::instance( $options );
