<?php if ( ! defined( 'ABSPATH' ) ) {
	die;
} // Cannot access pages directly.
/**
 * Framework admin enqueue style and scripts
 *
 * @since 1.0
 *
 */
if ( ! function_exists( 'wpl_wcs_admin_enqueue_scripts' ) ) {
	function wpl_wcs_admin_enqueue_scripts() {
		$current_screen        = get_current_screen();
		$the_current_post_type = $current_screen->post_type;
		if ( 'wpl_wcslider' == $the_current_post_type ) {

			// admin utilities.
			wp_enqueue_media();

			// wp core styles.
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'wp-jquery-ui-dialog' );

			// Framework Core Styles.
			wp_enqueue_style( 'sp-wcs-framework', WPL_WCS_URL . 'admin/views/metabox/assets/css/sp-framework.css', array(), WPL_WCS_VERSION );
			wp_enqueue_style( 'sp-wcs-custom', WPL_WCS_URL . 'admin/views/metabox/assets/css/sp-custom.css', array(), WPL_WCS_VERSION );
			wp_enqueue_style( 'sp-wcs-font-awesome' );
			wp_enqueue_style( 'wcs-style', WPL_WCS_URL . 'admin/views/metabox/assets/css/sp-style.css', array(), WPL_WCS_VERSION );

			if ( is_rtl() ) {
				wp_enqueue_style( 'sp-framework-rtl', WPL_WCS_URL . 'admin/views/metabox/assets/css/sp-framework-rtl.css', array(), WPL_WCS_VERSION );
			}

			// wp core scripts.
			wp_enqueue_script( 'wp-color-picker' );
			wp_enqueue_script( 'jquery-ui-dialog' );

			// framework core scripts.
			wp_enqueue_script( 'sp-wcs-plugins', WPL_WCS_URL . 'admin/views/metabox/assets/js/sp-plugins.js', array(), WPL_WCS_VERSION, true );
			wp_enqueue_script( 'sp-wcs-framework', WPL_WCS_URL . 'admin/views/metabox/assets/js/sp-framework.js', array( 'sp-wcs-plugins' ), WPL_WCS_VERSION, true );
		}

	}

	add_action( 'admin_enqueue_scripts', 'wpl_wcs_admin_enqueue_scripts' );
}
