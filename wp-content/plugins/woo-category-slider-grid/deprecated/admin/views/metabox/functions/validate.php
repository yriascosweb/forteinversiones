<?php if ( ! defined( 'ABSPATH' ) ) {
	die;
} // Cannot access pages directly.

/**
 * Numeric validate
 *
 * @since 1.0
 *
 */
if ( ! function_exists( 'wpl_wcs_validate_numeric' ) ) {
	function wpl_wcs_validate_numeric( $value, $field ) {

		if ( ! is_numeric( $value ) ) {
			return __( 'Please write a numeric data!', 'woo-category-slider' );
		}

	}
	add_filter( 'wpl_wcs_validate_numeric', 'wpl_wcs_validate_numeric', 10, 2 );
}

/**
 * Required validate
 *
 * @since 1.0
 *
 */
if ( ! function_exists( 'wpl_wcs_validate_required' ) ) {
	function wpl_wcs_validate_required( $value ) {
		if ( empty( $value ) ) {
			return __( 'Fatal Error! This field is required!', 'woo-category-slider' );
		}
	}
	add_filter( 'wpl_wcs_validate_required', 'wpl_wcs_validate_required' );
}
