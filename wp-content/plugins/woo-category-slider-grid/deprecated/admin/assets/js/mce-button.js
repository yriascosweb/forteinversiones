(function () {
    tinymce.PluginManager.add('wpl_wcs_mce_button', function (editor, url) {
        editor.addButton('wpl_wcs_mce_button', {
            text: false,
            icon: false,
            image: url + '/icon.png',
            tooltip: 'WooCommerce Category Slider',
            onclick: function () {
                editor.windowManager.open({
                    title: 'Insert Shortcode',
                    width: 400,
                    height: 100,
                    body: [
                        {
                            type: 'listbox',
                            name: 'listboxName',
                            label: 'Select Shortcode',
                            'values': editor.settings.wplWCSShortcodeList
                        }
                    ],
                    onsubmit: function (e) {
                        editor.insertContent('[woo_cat_slider id="' + e.data.listboxName + '"]');
                    }
                });
            }
        });
    });
})();