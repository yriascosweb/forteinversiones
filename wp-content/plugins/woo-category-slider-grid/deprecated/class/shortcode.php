<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class WPL_WCS_Shortcode {

	private static $_instance;

	/**
	 * WPL_WCS_Shortcode constructor.
	 */
	public function __construct() {
		//add_filter( 'init', array( $this, 'register_post_type' ) );
	}

	/**
	 * Allows for accessing single instance of class. Class should only be constructed once per call.
	 *
	 * @return WPL_WCS_Shortcode
	 */
	public static function getInstance() {
		if ( ! self::$_instance ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Register Post Type for Category Slider
	 */
	// public function register_post_type() {
	// 	register_post_type(
	// 		'wpl_wcslider',
	// 		array(
	// 			'label'           => __( 'WooCommerce Category Sliders', 'woo-category-slider' ),
	// 			'description'     => __( 'Generate WooCommerce Category Sliders', 'woo-category-slider' ),
	// 			'public'          => false,
	// 			'has_archive'     => false,
	// 			'publicaly_queryable'   => false,
	// 			'show_ui'         => true,
	// 			'show_in_menu'    => true,
	// 			'menu_icon'       => WPL_WCS_URL . 'admin/assets/images/icon.png',
	// 			'hierarchical'    => false,
	// 			'query_var'       => false,
	// 			'supports'        => array( 'title' ),
	// 			'capability_type' => 'post',
	// 			'labels'          => array(
	// 				'name'               => __( 'Category Sliders', 'woo-category-slider' ),
	// 				'singular_name'      => __( 'Category Slider', 'woo-category-slider' ),
	// 				'menu_name'          => __( 'Woo Cat Slider', 'woo-category-slider' ),
	// 				'add_new'            => __( 'Add New', 'woo-category-slider' ),
	// 				'add_new_item'       => __( 'Add New Slider', 'woo-category-slider' ),
	// 				'edit'               => __( 'Edit', 'woo-category-slider' ),
	// 				'edit_item'          => __( 'Edit Slider', 'woo-category-slider' ),
	// 				'new_item'           => __( 'New Slider', 'woo-category-slider' ),
	// 				'view'               => __( 'View Slider', 'woo-category-slider' ),
	// 				'view_item'          => __( 'View Slider', 'woo-category-slider' ),
	// 				'search_items'       => __( 'Search Slider', 'woo-category-slider' ),
	// 				'not_found'          => __( 'No Category Slider Found', 'woo-category-slider' ),
	// 				'not_found_in_trash' => __( 'No Category Slider Found in Trash', 'woo-category-slider' ),
	// 				'parent'             => __( 'Parent Category Slider', 'woo-category-slider' ),
	// 			)
	// 		)
	// 	);
	// }
}
