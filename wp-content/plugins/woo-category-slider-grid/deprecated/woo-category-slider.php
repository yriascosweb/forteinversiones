<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Handles core plugin hooks and action setup.
 *
 * @package woo-category-slider
 *
 * @since 1.0
 */
if ( ! class_exists( 'WPL_Woo_Category_Slider' ) ) {
	class WPL_Woo_Category_Slider {
		/**
		 * Plugin version
		 *
		 * @var string
		 */
		public $version = '1.1.3';

		/**
		 * @var WPL_WCS_Shortcode $shortcode
		 */
		public $shortcode;

		/**
		 * @var WPL_WCS_Router $router
		 */
		public $router;

		/**
		 * @var null
		 * @since 1.0
		 */
		protected static $_instance = null;

		/**
		 * @return WPL_Woo_Category_Slider
		 * @since 1.0
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}

			return self::$_instance;
		}

		/**
		 * Constructor.
		 */
		function __construct() {
			// Define constants.
			$this->define_constants();

			// Initialize the action hooks.
			$this->init_actions();

			// Initialize the filter hooks.
			$this->init_filters();

			// Required class file include.
			spl_autoload_register( array( $this, 'autoload' ) );

			// Include required files.
			$this->includes();

			// instantiate classes.
			$this->instantiate();

		}

		/**
		 * Define constants
		 *
		 * @since 1.0
		 */
		public function define_constants() {
			$this->define( 'WPL_WCS_VERSION', $this->version );
			$this->define( 'WPL_WCS_PATH', plugin_dir_path( __FILE__ ) );
			$this->define( 'WPL_WCS_URL', plugin_dir_url( __FILE__ ) );
			$this->define( 'WPL_WCS_BASENAME', plugin_basename( __FILE__ ) );
		}

		/**
		 * Define constant if not already set
		 *
		 * @param string $name
		 * @param string|bool $value
		 */
		public function define( $name, $value ) {
			if ( ! defined( $name ) ) {
				define( $name, $value );
			}
		}

		/**
		 * Initialize WordPress action hooks
		 *
		 * @return void
		 */
		public function init_actions() {
			add_action( 'plugins_loaded', array( $this, 'load_text_domain' ) );
			//add_action( 'manage_wpl_wcslider_posts_custom_column', array( $this, 'add_shortcode_form' ), 10, 2 );
		}

		/**
		 * Initialize WordPress filter hooks
		 *
		 * @return void
		 */
		function init_filters() {
			//add_filter( 'plugin_action_links', array( $this, 'add_plugin_action_links' ), 10, 2 );
			//add_filter( 'manage_wpl_wcslider_posts_columns', array( $this, 'add_shortcode_column' ) );
		}

		/**
		 * Load TextDomain for plugin.
		 *
		 */
		public function load_text_domain() {
			load_textdomain( 'woo-category-slider', WP_LANG_DIR . '/woo-category-slider-grid/languages/woo-category-slider-' . apply_filters( 'plugin_locale', get_locale(), 'woo-category-slider' ) . '.mo' );
			load_plugin_textdomain( 'woo-category-slider', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
		}

		/**
		 * Add plugin action menu
		 *
		 * @param array $links
		 * @param string $file
		 *
		 * @return array
		 */
		// public function add_plugin_action_links( $links, $file ) {

		// 	if ( WPL_WCS_BASENAME == $file ) {
		// 		$new_links = sprintf( '<a href="%s">%s</a>', admin_url( 'edit.php?post_type=wpl_wcslider' ), __( 'Add Slider', 'woo-category-slider' ) );

		// 		array_unshift( $links, $new_links );

		// 		$links['go_pro'] = sprintf( '<a target="_blank" href="%1$s" style="color: #35b747; font-weight: 700;">Go Premium!</a>', 'https://shapedplugin.com/plugin/woocommerce-category-slider-pro/' );
		// 	}

		// 	return $links;
		// }

		/**
		 * Autoload class files on demand
		 *
		 * @param string $class requested class name
		 */
		public function autoload( $class ) {
			$name = explode( '_', $class );
			if ( isset( $name[2] ) ) {
				$class_name = strtolower( $name[2] );
				$filename   = WPL_WCS_PATH . '/class/' . $class_name . '.php';

				if ( file_exists( $filename ) ) {
					require_once $filename;
				}
			}
		}

		/**
		 * Instantiate all the required classes
		 *
		 * @since 1.0
		 */
		public function instantiate() {
			$this->shortcode = WPL_WCS_Shortcode::getInstance();

			do_action( 'wpl_wcs_instantiate', $this );
		}

		/**
		 * Page router instantiate
		 *
		 * @since 1.0
		 */
		public function page() {
			$this->router = WPL_WCS_Router::instance();

			return $this->router;
		}

		/**
		 * Include the required files
		 *
		 * @return void
		 */
		public function includes() {
			$this->page()->wpl_wcs_function();
			$this->page()->wpl_wcs_metabox();
			$this->router->includes();
		}

		/**
		 * ShortCode Column
		 *
		 * @param $columns
		 *
		 * @return array
		 */
		// public function add_shortcode_column() {
		// 	$new_columns['cb']        = '<input type="checkbox" />';
		// 	$new_columns['title']     = __( 'Title', 'woo-category-slider' );
		// 	$new_columns['shortcode'] = __( 'Shortcode', 'woo-category-slider' );
		// 	$new_columns['']          = '';
		// 	$new_columns['date']      = __( 'Date', 'woo-category-slider' );

		// 	return $new_columns;
		// }

		/**
		 * ShortCode Column Form
		 *
		 * @param $column
		 * @param $post_id
		 */
		// public function add_shortcode_form( $column, $post_id ) {
		// 	switch ( $column ) {
		// 		case 'shortcode':
		// 			$column_field = '<input style="width: 230px;padding: 6px;" type="text" onClick="this.select();" readonly="readonly" value="[woo_cat_slider ' . 'id=&quot;' . $post_id . '&quot;' . ']"/>';
		// 			echo $column_field;
		// 			break;
		// 		default:
		// 			break;

		// 	} // end switch
		// }

	}
}

/**
 * Returns the main instance.
 *
 * @since 1.0
 *
 * @return WPL_Woo_Category_Slider
 */
function wpl_woo_category_slider() {
	return WPL_Woo_Category_Slider::instance();
}


/**
 * Wpl_woo_category_slider instance.
 */
wpl_woo_category_slider();
