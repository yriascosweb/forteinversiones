<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}// if direct access

/**
 * Functions
 */
class WPL_WCS_Functions {

	/**
	 * Initialize the class
	 */
	public function __construct() {
		//add_filter( 'post_updated_messages', array( $this, 'post_update_message' ) );
		//add_filter( 'admin_footer_text', array( $this, 'admin_footer' ), 1, 2 );
		// Post thumbnails.
		add_image_size( 'wpl-wcs-cat-img', 360, 400, true );
		add_image_size( 'wpl-wcs-cat-img-two', 300, 170, true );
		add_action( 'in_admin_header', array( $this, 'wpl_wcs_admin_banner' ) );
		add_filter( 'screen_options_show_screen', array( $this, 'wpl_wcs_remove_screen_options' ), 10, 2 );
	}

	/**
	 * Post update messages for Shortcode Generator
	 */
	// function post_update_message( $message ) {
	// 	$screen = get_current_screen();
	// 	if ( 'wpl_wcslider' == $screen->post_type ) {
	// 		$message['post'][1]  = $title = esc_html__( 'Slider updated.', 'woo-category-slider' );
	// 		$message['post'][4]  = $title = esc_html__( 'Slider updated.', 'woo-category-slider' );
	// 		$message['post'][6]  = $title = esc_html__( 'Slider published.', 'woo-category-slider' );
	// 		$message['post'][8]  = $title = esc_html__( 'Slider submitted.', 'woo-category-slider' );
	// 		$message['post'][10] = $title = esc_html__( 'Slider draft updated.', 'woo-category-slider' );
	// 	}

	// 	return $message;
	// }


	/**
	 * Review Text
	 *
	 * @return string
	 */
	// public function admin_footer( $text ) {
	// 	$screen = get_current_screen();
	// 	if ( 'wpl_wcslider' == $screen->post_type ) {
	// 		$url  = 'https://wordpress.org/support/plugin/woo-category-slider-grid/reviews/?filter=5#new-post';
	// 		$text = sprintf( __( 'If you like <strong>WooCommerce Category Slider</strong> please leave us a <a href="%s" target="_blank">&#9733;&#9733;&#9733;&#9733;&#9733;</a> rating. Your Review is very important to us as it helps us to grow more. ', 'woo-category-slider'), $url);
	// 	}

	// 	return $text;
	// }

	function wpl_wcs_admin_banner() {
		$screen = get_current_screen();
		if ( 'wpl_wcslider' == $screen->post_type ) { ?>
		<div class="wpl-wcs-framework wpl-wcs-banner-area">
			<div class="wpl-wcs-banner">
				<div class="wpl-wcs-logo"><img src="<?php echo WPL_WCS_URL . 'admin/assets/images/wcs-logo.png' ?>" alt="WooCommerce Category Slider"></div>
				<div class="wpl-wcs-short-links">
					<a href="https://shapedplugin.com/support-forum/" target="_blank"><span><i class="fa fa-question"></i></span>Help</a>
				</div>
			</div>
		</div>
		<?php }
	}

	function wpl_wcs_remove_screen_options( $display_boolean, $wp_screen_object ) {
		$screen = get_current_screen();
		if ( 'wpl_wcslider' == $screen->post_type ) {
			$wp_screen_object->render_screen_layout();
			$wp_screen_object->render_per_page_options();
			return false;
		}
		return true;
	}

}

new WPL_WCS_Functions();
