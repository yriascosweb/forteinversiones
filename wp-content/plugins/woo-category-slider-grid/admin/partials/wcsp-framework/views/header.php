<?php if ( ! defined( 'ABSPATH' ) ) {
	die; } // Cannot access directly.

  $demo    = get_option( 'spf_demo_mode', false );
  $text    = ( ! empty( $demo ) ) ? 'Deactivate' : 'Activate';
  $status  = ( ! empty( $demo ) ) ? 'deactivate' : 'activate';
  $class   = ( ! empty( $demo ) ) ? ' spf-warning-primary' : '';
  $section = ( ! empty( $_GET['section'] ) ) ? $_GET['section'] : 'about';
  $links   = array(
	  'about'           => 'About',
	  'quickstart'      => 'Quick Start',
	  'documentation'   => 'Documentation',
	  'free-vs-premium' => 'Free vs Premium',
	  'support'         => 'Support',
	  'relnotes'        => 'Release Notes',
  );

	?>
<div class="spf-welcome spf-welcome-wrap">

  <h1>Welcome to Codestar Framework v<?php echo SP_WCS::$version; ?></h1>

  <p class="spf-about-text">A Simple and Lightweight WordPress Option Framework for Themes and Plugins</p>

  <p class="spf-demo-button"><a href="<?php echo add_query_arg( array( 'spf-demo' => $status ) ); ?>" class="button button-primary<?php echo $class; ?>"><?php echo $text; ?> Demo</a></p>

  <div class="spf-logo">
	<div class="spf--effects"><i></i><i></i><i></i><i></i></div>
	<div class="spf--wp-logos">
	  <div class="spf--wp-logo"></div>
	  <div class="spf--wp-plugin-logo"></div>
	</div>
	<div class="spf--text">Codestar Framework</div>
	<div class="spf--text spf--version">v<?php echo SP_WCS::$version; ?></div>
  </div>

  <h2 class="nav-tab-wrapper wp-clearfix">
	<?php
	foreach ( $links as $key => $link ) {

		if ( SP_WCS::$premium && $key === 'free-vs-premium' ) {
			continue; }

		$activate = ( $section === $key ) ? ' nav-tab-active' : '';

		echo '<a href="' . add_query_arg(
			array(
				'page'    => 'spf-welcome',
				'section' => $key,
			),
			admin_url( 'tools.php' )
		) . '" class="nav-tab' . $activate . '">' . $link . '</a>';

	}
	?>
  </h2>
