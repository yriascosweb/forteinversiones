<?php if ( ! defined( 'ABSPATH' ) ) {
	die; } // Cannot access directly.
/**
 *
 * Setup Framework Class
 *
 * @since 1.0.0
 * @version 1.0.0
 */
if ( ! class_exists( 'SP_WCS_Welcome' ) ) {
	class SP_WCS_Welcome {

		private static $instance = null;

		public function __construct() {

			if ( SP_WCS::$premium && ( ! SP_WCS::is_active_plugin( 'codestar-framework/codestar-framework.php' ) || apply_filters( 'spf_welcome_page', true ) === false ) ) {
				return; }

			add_action( 'admin_menu', array( &$this, 'add_about_menu' ), 0 );
			add_filter( 'plugin_action_links', array( &$this, 'add_plugin_action_links' ), 10, 5 );
			add_filter( 'plugin_row_meta', array( &$this, 'add_plugin_row_meta' ), 10, 2 );

			$this->set_demo_mode();

		}

		// instance.
		public static function instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}
			return self::$instance;
		}

		public function add_about_menu() {
			add_management_page( 'Codestar Framework', 'Codestar Framework', 'manage_options', 'spf-welcome', array( &$this, 'add_page_welcome' ) );
		}

		public function add_page_welcome() {

			$section = ( ! empty( $_GET['section'] ) ) ? $_GET['section'] : '';

			SP_WCS::include_plugin_file( 'views/header.php' );

			// safely include pages.
			switch ( $section ) {

				case 'quickstart':
					SP_WCS::include_plugin_file( 'views/quickstart.php' );
					break;

				case 'documentation':
					SP_WCS::include_plugin_file( 'views/documentation.php' );
					break;

				case 'relnotes':
					SP_WCS::include_plugin_file( 'views/relnotes.php' );
					break;

				case 'support':
					SP_WCS::include_plugin_file( 'views/support.php' );
					break;

				case 'free-vs-premium':
					SP_WCS::include_plugin_file( 'views/free-vs-premium.php' );
					break;

				default:
					SP_WCS::include_plugin_file( 'views/about.php' );
					break;

			}

			SP_WCS::include_plugin_file( 'views/footer.php' );

		}

		public static function add_plugin_action_links( $links, $plugin_file ) {

			if ( $plugin_file === 'codestar-framework/codestar-framework.php' && ! empty( $links ) ) {
				$links['spf--welcome'] = '<a href="' . admin_url( 'tools.php?page=spf-welcome' ) . '">Settings</a>';
				if ( ! SP_WCS::$premium ) {
					$links['spf--upgrade'] = '<a href="http://codestarframework.com/">Upgrade</a>';
				}
			}

			return $links;

		}

		public static function add_plugin_row_meta( $links, $plugin_file ) {

			if ( $plugin_file === 'codestar-framework/codestar-framework.php' && ! empty( $links ) ) {
				$links['spf--docs'] = '<a href="http://codestarframework.com/documentation/" target="_blank">Documentation</a>';
			}

			return $links;

		}

		public function set_demo_mode() {

			$demo_mode = get_option( 'spf_demo_mode', false );

			if ( ! empty( $_GET['spf-demo'] ) ) {
				$demo_mode = ( $_GET['spf-demo'] === 'activate' ) ? true : false;
				update_option( 'spf_demo_mode', $demo_mode );
			}

			if ( ! empty( $demo_mode ) ) {
				SP_WCS::include_plugin_file( 'samples/options.samples.php' );

				if ( SP_WCS::$premium ) {

					SP_WCS::include_plugin_file( 'samples/customize-options.samples.php' );
					SP_WCS::include_plugin_file( 'samples/metabox.samples.php' );
					SP_WCS::include_plugin_file( 'samples/shortcoder.samples.php' );
					SP_WCS::include_plugin_file( 'samples/taxonomy-options.samples.php' );
					SP_WCS::include_plugin_file( 'samples/widgets.samples.php' );

				}
			}

		}

	}

	SP_WCS_Welcome::instance();
}
