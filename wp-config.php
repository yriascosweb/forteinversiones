<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'forte' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '8ljhxkrnbg5eqqlwnukilvk3hm7gh2tjww2z4iusemc8mcyiqdxtgu6r4vnxbnhz' );
define( 'SECURE_AUTH_KEY',  'zocknd2t6pvqnyzmvgrdgagmfsrkt7gtjadt0ma4ighyorm1wgseyrcexmkmd98f' );
define( 'LOGGED_IN_KEY',    'hogatjarfxyeukjcwhq3zclhjcauxnymfhlcv0qqdvz5gk0vok3ougspfcxd5yqq' );
define( 'NONCE_KEY',        'wklnt39o0zpqmayyceo9dhw9re9gwqk4h47mpcpgpjvow8kbvnpnd38aqg0maymn' );
define( 'AUTH_SALT',        'cjkutj5ucrb2ryqd9re0hkvuwqcgaa9getbcnlrcd7lnlazu6mbcnxghwnfyjodi' );
define( 'SECURE_AUTH_SALT', 'esjumxvtshle5c3pe3usj8fngvr2psbccdsuxsiccyohhzhzqxn6oewovhwkzqt8' );
define( 'LOGGED_IN_SALT',   'm5utlaqs4phjvjjgabmfhanfox4lrxos4rw7gu7xxf82fjgi1orimf4gw4ykdv3p' );
define( 'NONCE_SALT',       'kjl58lrwaber5kll1j37itcbabkzadduf8pogzpuuujj4l4y77gmx3cgzxmtenex' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpx8_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
